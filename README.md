# Api
[![codecov](https://codecov.io/gl/douglassantosdev/vma-api/branch/master/graph/badge.svg)](https://codecov.io/gl/douglassantosdev/vma-api)

[![codebeat badge](https://codebeat.co/badges/d443ad30-e080-44ed-9025-9305cb6ccfec)](https://codebeat.co/projects/gitlab-com-douglassantosdev-vma-api-test-master)

This is the VMA API built witn Nodejs and Mysql.


# install NodeJs dependencies

`npm i`

# Database setup

- Access your mysql server and type `create database vma;`.
- Clone this project to your local computer.
- Set your database credentials to `src/database/config/config.js` on `development` scope
- type `npm run migrate-dev` in the project root folder
- `git checkout src/database/config/config.js`, the `config.js` modification was done just execute the sequelize client.

# Environment Variables

- `cp .env.dist .env` on root folder.
- Set your database credentials to `.env`

`ORIGIN`: The origin allowed to perform requests on this API
`PORT`: Port for this API 

Default values on `.env.dist` you will access this api on localhost:5000 and accept request from http://localhost:3000 (FrontEndProject)

# Run

`npm start`

# Run tests

* all tests: `npm tests`
* specific test: `npm run tests-spec $path_to_test_file.ts`
