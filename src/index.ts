const bodyParser = require('body-parser');
import express = require('express');
import { cors, apiKeyCheck } from "./middlewares";
import { createUser, updateUser, deleteUser, listUsers, getUserById } from './controllers/user.controller';

const app = express();
app.use(cors({ origin: process.env.ORIGIN }));
app.use(bodyParser.json())
app.use(apiKeyCheck);

app.post('/v1/user', createUser);
app.put('/v1/user', updateUser);
app.delete('/v1/user/:id', deleteUser);
app.get('/v1/users', listUsers);
app.get('/v1/user/:id', getUserById);

app.listen(process.env.PORT)
console.log(`listening on port ${process.env.PORT}`)

module.exports = app ;
