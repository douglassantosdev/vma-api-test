import { UserDto } from '../dtos/user.dto';
import { UserAddressDto } from '../dtos/user.address.dto';
const db  = require('../database/models');

export const createUserOnDataBase = async (user: UserDto): Promise<UserDto> => {
    try {
        const transactionResult = await db.sequelize.transaction(async (t: any) => {
            const userResult = await db.User.create({
                name: user.getName(),
                age: user.getAge(),
                email: user.getEemail(),
                password: user.getPassword()
            }, { transaction: t });

            const addressResult = await db.UserAddress.create({
                userId: userResult.id,
                country: user.getAddress().getCountry(),
                state: user.getAddress().getState(),
                city: user.getAddress().getCity(),
                street: user.getAddress().getStreet(),
                number: user.getAddress().getNumber(),
                zipCode: user.getAddress().getZipCode()
            }, { transaction: t });

            return new UserDto(
                userResult.name,
                userResult.age,
                userResult.email,
                userResult.password,
                new UserAddressDto(
                    addressResult.country, 
                    addressResult.state, 
                    addressResult.city, 
                    addressResult.street, 
                    addressResult.number, 
                    addressResult.zipCode, 
                    addressResult.id, 
                    addressResult.createdAt, 
                    addressResult.updatedAt
                ),
                userResult.id,
                new Date(userResult.createdAt),
                new Date(userResult.updatedAt)
            );
        });

        return transactionResult;
    } catch (error) {
        throw error
    }
}

export const updateUserOnDataBase = async (user: UserDto): Promise<UserDto> => {
    try {
        const transactionResult = await db.sequelize.transaction(async (t: any) => {
            const addressResult = await db.UserAddress.update(
                user.getAddress(), 
                { transaction: t, where:{ id: user.getAddress().getId()}}
            );

            await db.User.update({
                name: user.getName(),
                age: user.getAge(),
                email: user.getEemail(),
                password: user.getPassword(),
                addressId: addressResult.id,
            }, { transaction: t, where:{ id: user.getId()}});

            return user;
        });

        return transactionResult;
    } catch (error) {
        throw error
    }
}

export const findAllUsers = async (filters: any) => {
    const pageSize = !filters || !filters.pageSize || filters.pageSize < 1 ? 10 : parseInt(filters.pageSize);
    const offset = !filters || !filters.page || filters.page <= 1 ? 0 : (parseInt(filters.page)-1) * pageSize; 

    //stringify to Json parse is just to remove undefined from object
    const where = filters ? JSON.parse(JSON.stringify({name: filters.name, email: filters.email})) : {};

    const result = await db.User.findAndCountAll({
        where,    
        include: [{ model: db.UserAddress, as: 'UserAddresses' }],
        limit: pageSize,
        offset: offset
    });
    
    const users = result.rows.map((row: any) => { 
        return new UserDto(
            row.name,
            row.age,
            row.email,
            null,
            createAddress(row),
            row.id,
            new Date(row.createdAt),
            new Date(row.updatedAt)
        )
    });

    return { 
        'from': offset, 
        'to': result.count > offset+pageSize ? offset+pageSize : result.count, 
        'count': result.count, 
        'rows': users 
    };
}

export const findUserById = async (id: string): Promise<UserDto> => {
    const row = await db.User.findByPk(id,{ include: [{ model: db.UserAddress, as: 'UserAddresses' }] });
    if(!row) {
        return null;
    }

    return !row ? null : new UserDto(
        row.name,
        row.age,
        row.email,
        null,
        createAddress(row),
        row.id,
        new Date(row.createdAt),
        new Date(row.updatedAt)
    )
}

export const destroyUserById = async (id: string): Promise<UserDto> => {
    return db.User.destroy({ where: { id }});
}

const createAddress = (row: any) => {
    return !row.UserAddresses[0] ? null : new UserAddressDto(
        row.UserAddresses[0].country,
        row.UserAddresses[0].state,
        row.UserAddresses[0].city,
        row.UserAddresses[0].street,
        row.UserAddresses[0].number,
        row.UserAddresses[0].zipCode,
        row.UserAddresses[0].id,
        row.UserAddresses[0].createdAt,
        row.UserAddresses[0].updatedAt
    );
}
