export const cors = require('cors');

export const apiKeyCheck = async (req: any, res: any, next: any) => {
  const apiKey = req.headers.authorization;
  if(!apiKey) {
    res.status(401).json({ "error": "Authorization required"});
  }

  //validate key, extract user etc .. and pass it along as part of the req object
  req.authUser = apiKey;
  next()
}
