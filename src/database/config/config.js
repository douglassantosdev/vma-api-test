require('dotenv').config();

module.exports = {
  "ci": {
    "logging": false,
    "username": 'root',
    "password": 'vma',
    "database": 'vma',
    "host": 'mysql',
    "dialect": "mysql",
    "operatorsAliases": false
  },
  "development": {
    "logging": false,
    "username": process.env.USERNAME,
    "password": process.env.PASSWORD,
    "database": process.env.DATABASE,
    "host": process.env.HOST,
    "dialect": "mysql",
    "operatorsAliases": false
  },
  "staging": {
    "logging": false,
    "username": process.env.USERNAME,
    "password": process.env.PASSWORD,
    "database": process.env.DATABASE,
    "host": process.env.HOST,
    "dialect": "mysql",
    "operatorsAliases": false
  },
  "production": {
    "username": process.env.USERNAME,
    "password": process.env.PASSWORD,
    "database": process.env.DATABASE,
    "host": process.env.HOST,
    "dialect": "mysql",
    "operatorsAliases": false
  }
}

