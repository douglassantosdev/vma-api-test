import { UserAddressDto } from "./user.address.dto";

export class UserDto {  

    private id: number;
    private name: string;
    private age: number;
    private email: string;
    private password: string;
    private address: UserAddressDto;
    private createdAt: Date;
    private updatedAt: Date;

    constructor( 
        name: string, 
        age: number, 
        email: string, 
        password: string, 
        address: UserAddressDto,
        id?: number,
        createdAt?: Date, 
        updatedAt?: Date
    ) {
        this.name = name;
        this.age = age;
        this.email = email;
        this.password = password;
        this.address = address;
        this.id = id;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

	public getId = (): number => {
		return this.id;
	}

	public getName = (): string => {
		return this.name;
	}

	public getAge = (): number => {
		return this.age;
	}

	public getEemail = (): string => {
		return this.email;
	}

	public getPassword = (): string => {
		return this.password;
	}

	public getAddress = (): UserAddressDto => {
		return this.address;
	}

	public getCreatedAt = (): Date => {
		return this.createdAt;
	}

	public getUpdatedAt = (): Date => {
		return this.updatedAt;
    }
    
    public static validateFields = (request: any) => {
        const errors = [];
        if(!request.name) {
            errors.push({"field":"name" , "msg": "name is required"});
        }
    
        if(!request.age) {
            errors.push({"field":"age" , "msg": "age is required"});
        }
    
        if(!request.email) {
            errors.push({"field":"email" , "msg": "email is required"});
        }
    
        if(!request.password) {
            errors.push({"field":"password" , "msg": "password is required"});
        }
    
        if(!request.address) {
            errors.push({"field":"address" , "msg": "address is required"});

            return errors;
        }

        if(!request.address.country) {
            errors.push({"field":"address.country" , "msg": "country is required"});
        }

        if(!request.address.city) {
            errors.push({"field":"address.city" , "msg": "city is required"});
        }

        if(!request.address.state) {
            errors.push({"field":"address.state" , "msg": "state is required"});
        }

        if(!request.address.street) {
            errors.push({"field":"address.street" , "msg": "street is required"});
        }

        if(!request.address.number) {
            errors.push({"field":"address.number" , "msg": "number is required"});
        }

        if(!request.address.zipCode) {
            errors.push({"field":"address.zipCode" , "msg": "zipCode is required"});
        }

        return errors;
    }
}
