export class UserAddressDto {  

    private id: number;
    private country: string;
    private state: string;
    private city: string;
    private street: string;
    private number: number;
    private zipCode: string;
    private createdAt: Date;
    private updatedAt: Date;

    constructor( 
        country: string,
        state: string,
        city: string,
        street: string,
        number: number,
        zipCode: string,
        id?: number,
        createdAt?: Date, 
        updatedAt?: Date
    ) {
        this.country = country;
        this.state = state;
        this.city = city;
        this.street = street;
        this.number = number;
        this.zipCode = zipCode;
        this.id = id;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public getId = (): number => {
        return this.id;
    }

    public getCountry = (): string => {
        return this.country;
    }

    public getState = (): string => {
        return this.state;
    }

    public getCity = (): string => {
        return this.city;
    }

    public getStreet = (): string => {
        return this.street;
    }

    public getNumber = (): number => {
        return this.number;
    }

    public getZipCode = (): string => {
        return this.zipCode;
    }

    public getCreatedAt = (): Date => {
        return this.createdAt;
    }

    public getUpdatedAt = (): Date => {
        return this.updatedAt;
    }
}
