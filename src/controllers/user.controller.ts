import { UserDto } from "../dtos/user.dto";
import { UserAddressDto } from "../dtos/user.address.dto";
import { 
    createUserOnDataBase, 
    updateUserOnDataBase, 
    findAllUsers, 
    findUserById,
    destroyUserById
 } from "../repository/user.repository";
const md5 = require('md5');

const BAD_REQUEST_HTTP_CODE = 400;
const CREATED_HTTP_CODE = 201;
const SUCCESS_HTTP_CODE = 200;

export const createUser = async (req:any , res: any) => {
    const body = req.body;
    if(!body) {
        res.status(BAD_REQUEST_HTTP_CODE).json({"error": "invalid request"});
        return;
    }

    const errors = UserDto.validateFields(body);
    if(errors.length > 0) {
        return res.status(BAD_REQUEST_HTTP_CODE).json(errors);
    }

    const user = await createUserOnDataBase(
        new UserDto(
            body.name, 
            body.age, 
            body.email, 
            md5(body.password), 
            new UserAddressDto(
                body.address.country,
                body.address.state,
                body.address.city,
                body.address.street,
                body.address.number,
                body.address.zipCode
            )
        )
    );

    return res.status(CREATED_HTTP_CODE).json(user);
}

export const updateUser = async (req:any , res: any) => {
    const body = req.body;
    if(!body) {
        res.status(BAD_REQUEST_HTTP_CODE).json({"error": "invalid request"});
        return;
    }

    if(!body.id) {
        res.status(BAD_REQUEST_HTTP_CODE).json({"field":"id", "id": "userId is required"});

        return;
    }

    const errors = UserDto.validateFields(body);
    if(errors.length > 0) {
        return res.status(BAD_REQUEST_HTTP_CODE).json(errors);
    }

    const user = await updateUserOnDataBase(
        new UserDto( 
            body.name, 
            body.age, 
            body.email, 
            md5(body.password),
            new UserAddressDto(
                body.address.country,
                body.address.state,
                body.address.city,
                body.address.street,
                body.address.number,
                body.address.zipCode,
                body.address.id
            ),
            body.id
        )
    );

    return res.status(SUCCESS_HTTP_CODE).json(user);
}

export const deleteUser = async (req:any , res: any) => {
    const body = req.body;
    if(!body) {
        res.status(BAD_REQUEST_HTTP_CODE).json({"error": "invalid request"});

        return;
    }

    await destroyUserById(req.params.id);

    res.json({ id: body.id });
}

export const listUsers = async (req:any , res: any) => {
    const result = await findAllUsers(req.query);

    res
      .set({
        'Content-Range': `bytes ${result.from}-${result.to}/${result.count}`, 
        'Access-Control-Expose-Headers': 'Content-Range'})
      .json(result.rows);
}

export const getUserById = async (req:any , res: any) => {
    const user = await findUserById(req.params.id);

    res.json(user);
}
