require('dotenv').config();
const should = require('should');
import { createUser, updateUser, deleteUser, listUsers, getUserById } from '../src/controllers/user.controller';

const BAD_REQUEST_HTTP_CODE = 400;

describe('Test user', async function () {
    this.timeout(30000  );
    let req = {
        body: {
            "id": null,
            "name": "douglas",
            "email": "douglas@gmail.com",
            "password": "test123",
            "age": 30,
            "address": {
                id: null,
                "country": "Brasil",
                "state": "RS",
                "city": "Porto Alegre",
                "street": "Eng. Antonio carlostibiriça",
                "number": 470,
                "zipCode": "90690040"
            }
        },
        params: { id: null }
    };

    const resGeneratorWithStatus = (callBack) => {
        return {
            status: (status) => {
                return {
                    json: (json) => {
                        callBack({ status, json })

                        return { status, json }
                    }
                }
            }
        };
    }

    const resGenerator = (callBack) => {
        return {
            json: (json) => {
                callBack({ json })

                return { json }
            }
        };
    }

    const resGeneratorWithPagination = (callBack) => {
        return {
            set: (h1) => {
                return {
                    json: (json) => {
                        callBack({ headers: [h1], json })

                        return { headers: [h1], json }
                    }
                }
            }
        };
    }

    let user = null;

    it('Test /v1/user create', async () => {
        const res = resGeneratorWithStatus((response) => { 
            should(req.body.name).be.equal(response.json.name);
            should(req.body.age).be.equal(response.json.age);
            should(req.body.email).be.equal(response.json.email);
            should(req.body.address.country).be.equal(response.json.address.country);
            should(req.body.address.city).be.equal(response.json.address.city);
            should(req.body.address.street).be.equal(response.json.address.street);
            should(req.body.address.number).be.equal(response.json.address.number);
            should(req.body.address.zipCode).be.equal(response.json.address.zipCode);
            should(response.json.id).be.not.equal(null);
            should(response.json.address.id).be.not.equal(null);

            should(req.body.password).be.not.equal(response.json.password);

            user = response.json
        });

        await createUser(req, res);
    });

    it('Test /v1/user create missing required field', async () => {
        const res = resGeneratorWithStatus((response) => { 
            should(response).be.deepEqual(
                { status: BAD_REQUEST_HTTP_CODE, json: [ { field: 'name', msg: 'name is required' } ] }
            );
        });
        req.body.name = null;

        await createUser(req, res);
    });

    it('Test /v1/user create with invalid request', async () => {
        const res = resGeneratorWithStatus((response) => { 
            should(response).be.deepEqual(
                { status: BAD_REQUEST_HTTP_CODE, json: { error: 'invalid request' } }
            );
        });
        req.body = null;

        await createUser(req, res);
    });

    it('Test /v1/user update', async () => {
        req.body =  {
            "id": user.id,
            "name": "name updated",
            "email": "updated_douglas@gmail.com",
            "password": "test123Updated",
            "age": 31,
            "address": {
                "id": user.address.id,
                "country": "Portugal",
                "state": "Santa catarina",
                "city": "Florianopolis",
                "street": "Test",
                "number": 471,
                "zipCode": "10690040"
            }
        }
        const res = resGeneratorWithStatus((response) => { 
            should(req.body.name).be.equal(response.json.name);
            should(req.body.age).be.equal(response.json.age);
            should(req.body.email).be.equal(response.json.email);
            should(req.body.address.country).be.equal(response.json.address.country);
            should(req.body.address.city).be.equal(response.json.address.city);
            should(req.body.address.street).be.equal(response.json.address.street);
            should(req.body.address.number).be.equal(response.json.address.number);
            should(req.body.address.zipCode).be.equal(response.json.address.zipCode);
            should(response.json.id).be.not.equal(null);
            should(response.json.address.id).be.not.equal(null);

            should(response.json.id).be.not.equal(null);

            user = response.json
        });

        await updateUser(req, res);
    });

    it('Test /v1/user update missing id', async () => {
        const res = resGeneratorWithStatus((response) => { 
            should(response).be.deepEqual(
                { status: BAD_REQUEST_HTTP_CODE, json: { field: 'id', id: 'userId is required' } }
            );
        });

        await updateUser({body: {name: 'test'}}, res);
    });

    it('Test /v1/user update missing required field', async () => {
        const res = resGeneratorWithStatus((response) => { 
            should(response).be.deepEqual({
                status: BAD_REQUEST_HTTP_CODE,
                json: [
                  { field: 'name', msg: 'name is required' },
                  { field: 'age', msg: 'age is required' },
                  { field: 'email', msg: 'email is required' },
                  { field: 'password', msg: 'password is required' },
                  { field: 'address', msg: 'address is required' }
                ]
            });
        });

        await updateUser({body: {id: 1}}, res);
    });

    it('Test /v1/user update with invalid request', async () => {
        const res = resGeneratorWithStatus((response) => { 
            should(response).be.deepEqual(
                { status: BAD_REQUEST_HTTP_CODE, json: { error: 'invalid request' } }
            );
        });

        await updateUser({body: null}, res);
    });

    it('Test /v1/users list', async () => {
        const res = resGeneratorWithPagination((response) => { 
            should(response.headers[0]['Content-Range']).be.not.equal(null);
            should(response.json.length).be.not.equal(0);
        });

        await listUsers(req, res);
    });

    it('Test /v1/user/:id get by id', async () => {
        const res = resGenerator((response) => { 
            should(response.json.id).be.equal(user.id);
        });
        req.params.id = user.id;

        await getUserById(req, res);
    });

    it('Test /v1/user/:id delete', async () => {
        const res = resGenerator((response) => { 
            should(response.json.id).be.equal(user.id);
        });
        req.params.id = user.id;

        await deleteUser(req, res);
    });

    it('Test /v1/user/:id delete with invalid request', async () => {
        const res = resGeneratorWithStatus((response) => { 
            should(response).be.deepEqual({ status: BAD_REQUEST_HTTP_CODE, json: { error: 'invalid request' } });
        });

        await deleteUser({body: null}, res);
    });
});
